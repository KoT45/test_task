import requests
import json
import argparse
import sys
from pprint import pprint

host = "127.0.0.1"
port = 8080


def get_tree(uuid):
    r = requests.get(f'http://{host}:{port}/{uuid}')
    return r

def put_tree(data):
    r = requests.put(f'http://{host}:{port}/', data=data)
    return r

def delete_tree(uuid):
    r = requests.delete(f'http://{host}:{port}/{uuid}')
    return r

def patch_tree(uuid, data):
    r = requests.patch(f'http://{host}:{port}/{uuid}', data=data)
    return r

def test():
    try:
        # check random correct uuid
        r = get_tree("f5f70a49-b37f-4cf6-aeaa-bd7b5a19f5a1")
        assert(r.status_code == 404)

        # check incorrect uuid
        r = get_tree("f5f70a49-b37f-4cf6-aeaa-bd7b")
        assert(r.status_code == 500)

        #check create tree
        with open('tree.json', 'r') as f:
            data = json.dumps(json.load(f))
        r = put_tree(data)
        assert(r.status_code == 200)
        tree_uuid = r.content.decode("utf-8")

        # check data
        r = get_tree(tree_uuid)
        assert(r.status_code == 200)
        assert(data.replace(' ', '') == r.content.decode('utf-8'))

        #check create tree with incorrect data
        with open('incorrect_tree.json', 'r') as f:
            data = json.dumps(json.load(f))
        r = put_tree(data)
        assert(r.status_code == 500)

        # check delete
        r = delete_tree(tree_uuid)
        assert(r.status_code == 200)

        # check delete with incorrect uuid
        r = delete_tree("f5f70a49-Asdjk12-4cf6-aeaa-bd7b")
        assert(r.status_code == 500)

        # check delete not existed tree
        r = delete_tree(tree_uuid)
        assert(r.status_code == 404)

        # check patch
        # create tree
        with open('tree.json', 'r') as f:
            data = json.dumps(json.load(f))
        r = put_tree(data)
        assert(r.status_code == 200)
        tree_uuid = r.content.decode("utf-8")
        with open('subtree1.json', 'r') as f:
            data = json.dumps(json.load(f))
        r = patch_tree(tree_uuid, data)
        assert(r.status_code == 200)
        assert(r.content.decode("utf-8") == "OK")

        r = patch_tree(tree_uuid, data)
        assert(r.status_code == 200)
        assert(r.content.decode("utf-8") == "DID NOT DELETE")

        # check modified
        r = get_tree(tree_uuid)
        assert(r.status_code == 200)
        with open('patched1_tree.json', 'r') as f:
            data = json.dumps(json.load(f))
        assert(data.replace(' ', '') == r.content.decode('utf-8'))

        # check subtree not found
        with open('subtree2.json', 'r') as f:
            data = json.dumps(json.load(f))
        r = patch_tree(tree_uuid, data)
        assert(r.status_code == 200)
        assert(r.content.decode("utf-8") == "DID NOT DELETE")

    except Exception as e:
        pprint(str(e))
        return "FAIL"

    return "OK"


def main():
    if len(sys.argv) > 1 and sys.argv[1].lower() == "test":
        res = test()
        print(res)
        if res == "OK":
            sys.exit(0)
        else:
            sys.exit(1)

    parser = argparse.ArgumentParser()
    parser.add_argument('http_method')
    parser.add_argument('file_or_uuid', nargs='+')
    args = parser.parse_args()
    http_method = args.http_method.upper()
    file_or_uuid = args.file_or_uuid

    if http_method == "GET":
        uuid = file_or_uuid[0]
        r = get_tree(uuid)

    elif http_method == "PUT":
        filename = file_or_uuid[0]
        with open(filename, 'r') as f:
            data = json.dumps(json.load(f))
        r = put_tree(data)

    elif http_method == "DELETE":
        uuid = file_or_uuid[0]
        r = delete_tree(uuid)

    elif http_method == "PATCH":
        uuid = file_or_uuid[0]
        filename = file_or_uuid[1]
        with open(filename, 'r') as f:
            data = json.dumps(json.load(f))
        r = patch_tree(uuid, data)

    else:
        parser.print_help()
        sys.exit(1)

    pprint(r.status_code)
    pprint(r.content)

if __name__ == "__main__":
    main()