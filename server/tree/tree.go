package tree

import (
	"github.com/google/uuid"
	//"sort"
	"fmt"
	"encoding/json"
	"log"
)

type Node struct {
	Value int
	Childs []*Node
	Parent *Node `json:"-"`
}

type Tree struct {
	Id uuid.UUID
	Root *Node
}

func NewTree(treeId uuid.UUID) *Tree {
	tree := new(Tree)
	tree.Id = treeId
	tree.Root = &Node{}
	return tree
}

func (n *Node) AddNewValue(v int) {
	newNode := Node{Value: v}
	n.Childs = append(n.Childs, &newNode)
}

func (n *Node) AddNewNode(newNode *Node) {
	n.Childs = append(n.Childs, newNode)
}

func (n *Node) findNodeWithValue(v int, p *Node) []*Node {
	var res []*Node
	if v == n.Value {
		n.Parent = p
		res = append(res, n)
	}
	for _, child := range n.Childs {
		res = append(res, child.findNodeWithValue(v, n)...)
	}
	return res
}

func (n *Node) PrintJson() {
	bytes, err := json.Marshal(n)
	if err != nil {
    	log.Println(err)
		return
	}
	fmt.Println(string(bytes))
}

func (n *Node) getChildValues() []int {
	var res []int
	for _, child := range n.Childs {
		res = append(res, child.Value)
	}
	return res
}

func valInChilds(nodes []*Node) []int {
	var res []int
    for _, n := range nodes {
        res = append(res, n.Value)
    }
    return res
}

func compareChildValues(s1 []int, s2 []int) bool {
	if len(s1) != len(s2) {
		return false
	}
	// assume that childs are ordered
	//sort.Ints(s1)
	//sort.Ints(s2)
	for i, _ := range s1 {
		if s1[i] != s2[i] {
			return false
		}
	}
	return true
}

func (n *Node) isEqual(test_n *Node) bool {
	// compare self values
	if n.Value != test_n.Value {
		return false
	}
	// compare childs
	if !compareChildValues(valInChilds(n.Childs), valInChilds(test_n.Childs)) {
		return false
	}
	// if n is leaf than n == test_n
	if len(n.Childs) == 0 {
		return true
	}

	// compare next subtrees
	for i, _ := range n.Childs {
		if !n.Childs[i].isEqual(test_n.Childs[i]) {
			return false
		}
	}

	return true
}

func (tree *Tree) DeleteSubtreeInTrees(test_n *Node) bool {
	n := tree.Root
	root_candidates := n.findNodeWithValue(test_n.Value, nil)

	for _, candidate := range root_candidates {
		if candidate.isEqual(test_n) {

			p := candidate.Parent
			if p == nil {
				tree.Root = nil
				return true
			}

			var i int
			for i, _ = range p.Childs {
				if p.Childs[i] == candidate {
					break
				}
			}
			p.Childs = append(p.Childs[:i], p.Childs[i+1:]...)

			return true
		}
	}

	return false
}
