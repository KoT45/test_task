package main

import (
	"./tree"
	//"fmt"
	"encoding/json"
	"log"
	"github.com/google/uuid"
	"github.com/gin-gonic/gin"
	"net/http"
	//"strings"
	//"strconv"
	//"sort"
)


var trees map[uuid.UUID]*tree.Tree = make(map[uuid.UUID]*tree.Tree)

func putTree(c *gin.Context) {
	id, err := uuid.NewRandom()
	if err != nil {
		log.Println(err)
		c.String(http.StatusInternalServerError, "Cannot create uuid")
		return
	}

	text_id, err := id.MarshalText()
	if err != nil {
		log.Println(err)
		c.String(http.StatusInternalServerError, "Cannot get text from uuid")
		return
	}

	bytes, err := c.GetRawData()
	if err != nil {
    	log.Println(err)
		c.String(http.StatusInternalServerError, "Cannot read request's body")
    	return
	}

	new_tree := tree.NewTree(id)
	err = json.Unmarshal(bytes, &new_tree.Root);
    if err != nil {
    	log.Println(err)
		c.String(http.StatusInternalServerError, "Cannot unmarshal recived data to struct")
    	return
	}

	trees[id] = new_tree

	c.String(http.StatusOK, string(text_id))
}

func getTree(c *gin.Context)  {
    var id uuid.UUID
    err := id.UnmarshalText([]byte(c.Param("uuid")))
    if err != nil {
    	log.Println(err)
		c.String(http.StatusInternalServerError, "Incorrect uuid format")
    	return
    }

    t, ok := trees[id]
    if !ok {
		c.AbortWithStatus(http.StatusNotFound)
    	return
    }

    c.JSON(http.StatusOK, t.Root)
}

func deleteTree(c *gin.Context) {
	var id uuid.UUID
    err := id.UnmarshalText([]byte(c.Param("uuid")))
    if err != nil {
    	log.Println(err)
		c.String(http.StatusInternalServerError, "Incorrect uuid format")
    	return
    }

    _, ok := trees[id]
    if !ok {
		c.AbortWithStatus(http.StatusNotFound)
    	return
    }
    delete(trees, id)
    c.AbortWithStatus(http.StatusOK)
}

func patchTree(c *gin.Context) {
	var id uuid.UUID
    err := id.UnmarshalText([]byte(c.Param("uuid")))
    if err != nil {
    	log.Println(err)
		c.String(http.StatusInternalServerError, "Incorrect uuid format")
    	return
    }

    _, ok := trees[id]
    if !ok {
		c.AbortWithStatus(http.StatusNotFound)
    	return
    }

    bytes, err := c.GetRawData()
	if err != nil {
    	log.Println(err)
		c.String(http.StatusInternalServerError, "Cannot read request's body")
    	return
	}

	sub_tree := new(tree.Node)
	err = json.Unmarshal(bytes, &sub_tree);
    if err != nil {
    	log.Println(err)
		c.String(http.StatusInternalServerError, "Cannot unmarshal recived data to struct")
    	return
	}

	res := trees[id].DeleteSubtreeInTrees(sub_tree)

	var return_message string
	if res {
		return_message = "OK"
	} else {
		return_message = "DID NOT DELETE"
	}
	c.String(http.StatusOK, return_message)
}

func main() {
	router := gin.Default()
   	router.PUT("/", putTree)
   	router.GET("/:uuid", getTree)
   	router.DELETE("/:uuid", deleteTree)
   	router.PATCH("/:uuid", patchTree)
   	router.Run(":8080")
}